package encontreonumero;

public class Conversao {

    public String converterFrase(String frase){
        String numeroCompleto = "";
        for (int i = 0; i < frase.length(); i++){
            String caractere = String.valueOf(frase.charAt(i));
            if (caractere.equalsIgnoreCase("-")){
                numeroCompleto += "-";
            }else{
                numeroCompleto += String.valueOf(converterCaractere (caractere));
            }
        }
        return numeroCompleto;
    }
    
    public int converterCaractere(String caractere) {
        int num = 0;
        switch (caractere) {
            case "A":
            case "B":
            case "C":
                num = 2;
                break;

            case "D":
            case "E":
            case "F":
                num = 3;
                break;

            case "G":
            case "H":
            case "I":
                num = 4;
                break;

            case "J":
            case "K":
            case "L":
                num = 5;
                break;

            case "M":
            case "N":
            case "O":
                num = 6;
                break;

            case "P":
            case "Q":
            case "R":
            case "S":
                num = 7;
                break;

            case "T":
            case "U":
            case "V":
                num = 8;
                break;

            case "W":
            case "X":
            case "Y":
            case "Z":
                num = 9;
                break;
                
            case "1":
                num = 1;
                break;
                
            case "0":
                num = 0;
                break;
        }
        return num;
    }
}
